<?php
/**
 * @file SemanticBlocksTest.php
 * @author Michael A. Sypes <michael@sypes.org>
 * @project d8_testbed
 *
 * @abstract
 */

namespace Drupal\Tests\SemanticBlocks;

use Drupal\semanticblocks\DataRetriever;
use Drupal\semanticblocks\Plugin\Block\SemanticBlock;
use Drupal\Tests\UnitTestCase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Prophecy\Argument;

/**
 * Class SemanticBlocksTest
 * @package Drupal\Tests\SemanticBlocks\Unit
 * @group SemanticBlocks
 */
class SemanticBlocksTest extends UnitTestCase {
	/**
	 * Given a regular array, a proper table array should be returned.
	 */
	public function testBlockCanOutputArray(){

		$data_retriever_prophecy = $this->prophesize(DataRetriever::class);
		$data_retriever_prophecy
			->retrieveData()
			->shouldBeCalled()
			->willReturn( [
				[
                    'Title' => 'Title 1',
                    'Time' => '10:00 AM',
                    'Author' => 'Author One'
                ],
                [
                    'Title' => 'Another Title',
                    'Time' => '11:30 AM',
                    'Author' => 'Author Two'
                ],
                [
                    'Title' => 'A Third Title',
                    'Time' => '1:00 PM',
                    'Author' => 'Author Three'
                ],
            ]
		);

		$data_retriever = $data_retriever_prophecy->reveal();

		// Base Block Constructor needs.
		$configuration = [
			'id' => "semanticblocks_block",
			'label' => "SemanticBlocks Block",
			'provider' => "semanticblocks",
			'label_display' => "visible"
		];
		$plugin_id = 'semanticblocks_block';
		$plugin_definition = [
			'admin_label' => $this->prophesize(TranslatableMarkup::class)->reveal(),
			'category' => "Semantic Blocks",
			'id' => "semanticblocks_block",
			'class' => "Drupal\semanticblocks\Plugin\Block\SemanticBlock",
			'provider' => "semanticblocks"
		];

		$block = new SemanticBlock($data_retriever, $configuration, $plugin_id, $plugin_definition);

		$output = $block->build();

		$this->assertArrayHasKey('#type', $output);
		$this->assertEquals('table', $output['#type']);
		$this->assertArrayHasKey('#header', $output);
		$this->assertTrue(is_array($output['#header']), 'Headers isn\'t an array');
		$this->assertArrayHasKey('#rows', $output);
		$this->assertTrue(is_array($output['#rows']), 'Rows isn\'t an array');
		$this->assertArrayNotHasKey('#empty');
	}

	/**
	 * Given a regular array, a proper table array should be returned.
	 */
	public function testBlockCanHandleEmptyArray(){

		$data_retriever_prophecy = $this->prophesize(DataRetriever::class);
		$data_retriever_prophecy
			->retrieveData()
			->shouldBeCalled()
			->willReturn( [] );
		$data_retriever = $data_retriever_prophecy->reveal();

		// Base Block Constructor needs.
		$configuration = [
			'id' => "semanticblocks_block",
			'label' => "SemanticBlocks Block",
			'provider' => "semanticblocks",
			'label_display' => "visible"
		];
		$plugin_id = 'semanticblocks_block';
		$plugin_definition = [
			'admin_label' => $this->prophesize(TranslatableMarkup::class)->reveal(),
			'category' => "Semantic Blocks",
			'id' => "semanticblocks_block",
			'class' => "Drupal\semanticblocks\Plugin\Block\SemanticBlock",
			'provider' => "semanticblocks"
		];

		$block = new SemanticBlock($data_retriever, $configuration, $plugin_id, $plugin_definition);

		$output = $block->build();

		$this->assertArrayHasKey('#type', $output);
		$this->assertEquals('table', $output['#type']);
		$this->assertArrayHasKey('#header', $output);
		$this->assertTrue(is_array($output['#header']), 'Headers isn\'t an array');
		$this->assertArrayHasKey('#rows', $output);
		$this->assertTrue(is_array($output['#rows']), 'Rows isn\'t an array');
		$this->assertArrayNotHasKey('#empty');
	}

}