This module was created on 2019-03-28 to meet a coding challenge presented by Semantic Bits.

The original specifics were accessed at
https://github.com/dkrylovsb/drupal-coding-challenge/blob/master/README.md

PHP/Drupal Coding Challenge

This is a small Drupal coding challenge that we normally ask all candidates to
complete in order to help us better assess their coding abilities. The challenge
 is designed to hopefully not take more than 1-2 hours of your time (assuming
   you have previous experience with Drupal). The challenge is also designed to
   be aligned with the PHP/Drupal/DKAN-based project on which you most likely
   will be working on. The instructions are as follows.

Please develop a custom Drupal module that will render a list of pages that were
 modified on a current day:

* Please develop a Block module
* Ensure your module shows up in the Module Listing
* Include a Drupal test case for your module
* Use Drupal 7 or 8

We do understand your time is valuable and therefore tried to keep the scope of
this challenge to 1-2 hours. The goal is to see your approach to the problem and
 your coding style rather than delivering a specific set of features.

Thank you for your interest in SemanticBits!

<hr>
Steps Taken:
Each major step is reflected in a git commit.

1. Made sure I could produce a simple block accessible through the regular UI.
2. Swapped out basic markup for a table render array.

    At this point, wrote a simple test. Got some nice failures along the way,
    and made a commit once everything was green.

3. Next level of abstraction was to get the array from another class.

    My intention was to create a service and use simple dependency injection,
    but Drupal's way of handling this for plugins requires use of their
    `static create()` function
    (see https://chromatichq.com/blog/dependency-injection-drupal-8-plugins),
    and I wanted to avoid using any more time than necessary.
    So on to an ugly instantiation of what I wanted to be the service class.

4. Refactored items to use in-place instantiation (ugh) and make a simple query.

There's plenty more than can/should be done.
* There's the aforementioned dependency injection issues.
* There's security. Yeah, I've got plain text DB creds exposed. (This is an old
  junk DB that only exists on my laptop, so it's not like someone's going to do
  anything with it.)
* There should be a `try/catch` for the DB call for graceful failure.
* It could be argued that the block is making too many assumptions about what's
returned by the DataRetriever.
