<?php
/**
 * @file DataRetriever.php
 * @author Michael A. Sypes <michael@sypes.org>
 * @project d8t
 *
 * @abstract
 */

namespace Drupal\semanticblocks;

use PDO;

class DataRetriever implements DataRetrieverInterface {

	private $db;

	public function __construct() {
		$this->db = new \PDO('mysql:host=localhost;dbname=wnbnetworkwest;charset=utf8mb4', 'wnbnetworkwest', 'XCVJ-=0b_e}A');
	}

	public function retrieveData() : array {
		$stmt = $this->db->query(
			'SELECT title, users.name AS author, users.d AS time
       		FROM books
       		JOIN users ON users.id = books.author
       		LIMIT 10');

		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
}