<?php

/**
 * @file
 * Contains \Drupal\semanticblocks\Plugin\Block\SemanticBlock.
 */

namespace Drupal\semanticblocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\semanticblocks\DataRetrieverInterface;
use Drupal\semanticblocks\InternalDataRetriever;
use Robo\State\Data;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'SemanticBlocks' block.
 *
 * @Block(
 *  id = "semanticblocks_block",
 *  admin_label = @Translation("SemanticBlocks Block"),
 * )
 */
class SemanticBlock extends BlockBase implements ContainerFactoryPluginInterface{

	protected $data_retriever;

	/**
	   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
	   * @param array $configuration
	   * @param string $plugin_id
	   * @param mixed $plugin_definition
	   *
	   * @return static
	   */
	  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
	    return new static(
	      $configuration,
	      $plugin_id,
	      $plugin_definition,
	      $container->get('semanticblocks.internal_data_retriever')
	    );
	  }

		/**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param Drupal\semanticblocks\DataRetrieverInterface $data_retriever
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DataRetrieverInterface $data_retriever) {
		$this->data_retriever = $data_retriever;
		parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

	/**
	 * {@inheritdoc}
	 */
	public function build() {
		// Get the simple array.
		$data = $this->getArray();

		$render_array = [
			'#type' => 'table'
		];

		if(!empty($data)) {
			$headers = array_keys($data[0]);

			// Capitalize Headers.
			array_walk($headers, function(&$item){
				$item = ucwords($item);
			});

			$render_array['#header'] = $headers;

			// Extract time for pretty display.
			if(isset($data[0]['time'])) {
				array_walk($data, function(&$item){
					if (preg_match('/^\d{10}$/', $item['time'])){
						$item['time'] = date('g:i A', $item['time']);
					}
					else {
						$item['time'] = date('g:i A', strtotime($item['time']));
					}
				});
			}

			$render_array['#rows'] = $data;
		}
		else {
			$render_array['#header'] = [];
			$render_array['#empty'] = 'No data returned';
		}

		return $render_array;
	}

	public function getArray(){

		// if(!$this->data_retriever instanceof InternalDataRetriever) {
		// 	$this->data_retriever = new InternalDataRetriever();
		// }

		return $this->data_retriever->retrieveData();
	}
}
