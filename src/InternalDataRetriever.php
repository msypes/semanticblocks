<?php
/**
 * @file InternalDataRetriever.php
 * @author Michael A. Sypes <michael@sypes.org>
 * @project d8t
 *
 * @abstract
 */

namespace Drupal\semanticblocks;

use Drupal;

class InternalDataRetriever implements DataRetrieverInterface {
	private $entityTypeManager;
	private $storage;
	private $query;

//	public function __construct($entityTypeManager) {
//		$this->entityTypeManager = $entityTypeManager;
//		$this->storage = $this->entityTypeManager->getStorage('node');
//		$this->query = $this->storage->getQuery();
//
//	}

	public function retrieveData() : array {
		$storage = \Drupal::getContainer()
		                 ->get('entity_type.manager')
		                 ->getStorage('node');

		$query = $storage->getQuery();

		$timestamp = strtotime(date('Y-m-d'));
		$nids = $query
			//->condition('type', 'page')
			->condition('changed', 1541099772, '>=')
			->execute();

		if (!$nids) {
			return [];
		}

		$nodes = $storage->loadMultiple($nids);
		//echo '<pre>' . print_r($nodes, 1) . '</pre>';die();

		$return_array = [];

		foreach ($nodes as $node){
			$return_array[] = [
				'Title' => $node->getTitle(),
				//'Author' => $node->getUser(),
				'time' => $node->changed->value
			];
		};

//echo '<pre>' . print_r($return_array, 1) . '</pre>';die();
		return $return_array;
	}
}
