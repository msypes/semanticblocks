<?php
/**
 * @file DataRetrieverInterface.php
 * @author Michael A. Sypes <michael@sypes.org>
 * @project d8t
 *
 * @abstract
 */

namespace Drupal\semanticblocks;

interface  DataRetrieverInterface {
	public function retrieveData() : array;
}